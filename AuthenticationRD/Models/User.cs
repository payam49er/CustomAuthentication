﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity;

namespace AuthenticationRD.Models
{
    public class User : IUser
    {

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        [NotMapped]
        public string PasswordHash { get; set; }

        [NotMapped]
        public string Id
        {
            get
            {
                return UserId;
            }
        }
    }
}
