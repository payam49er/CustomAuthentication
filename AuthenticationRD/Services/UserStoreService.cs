﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationRD.Context;
using AuthenticationRD.Models;
using Microsoft.AspNet.Identity;

namespace AuthenticationRD.Services
{
    public class UserStoreService : IUserStore<Models.User>, IUserPasswordStore<Models.User>
    {
        MyDbContext _context;

        public UserStoreService(MyDbContext Context)
        {
            _context = Context;
        }


        public Task CreateAsync( User user )
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync( User user )
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByIdAsync( string userId )
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByNameAsync( string userName )
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync( User user )
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose( bool disposing )
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UserStoreService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public Task SetPasswordHashAsync( User user, string passwordHash )
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync( User user )
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasPasswordAsync( User user )
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
