﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace AuthenticationRD.Services
{
    public class PasswordHasher : IPasswordHasher
    {
        public string HashPassword( string password )
        {
            //this is a demo, never use this in practice
            return password;
        }

        public PasswordVerificationResult VerifyHashedPassword( string hashedPassword, string providedPassword )
        {
            if (hashedPassword == HashPassword(providedPassword))
                return PasswordVerificationResult.Success;
            else
                return PasswordVerificationResult.Failed;
        }
    }
}
