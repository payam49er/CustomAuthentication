﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationRD.Context
{
   public class MyDbContext : DbContext
    {
        public MyDbContext():base("Defaultconnection")

        {
            Database.SetInitializer<MyDbContext>(null);
        }

        public DbSet<Models.User> Users { get; set; } 
    }
}
